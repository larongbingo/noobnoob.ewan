using NoobNoob.eWAN.Core.Base;

namespace NoobNoob.eWAN.Core;

public class User : Entity
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
}