using System;

namespace NoobNoob.eWAN.Core.Base;

public abstract class Entity
{
    public Guid Id { get; set; } = Guid.NewGuid();
}