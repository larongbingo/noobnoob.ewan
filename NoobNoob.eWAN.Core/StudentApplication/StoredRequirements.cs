using System.Linq;
using System.Collections.Generic;

namespace NoobNoob.eWAN.Core.StudentApplication;

/// <summary>
/// A collection of <see cref="StoredRequirement">StoredRequirement</see>
/// </summary>
public class StoredRequirements : List<StoredRequirement>
{
    public List<Requirement> GetMissingRequirements(List<Requirement> requirements)
        => this.Select(x => x.Requirement).Except(requirements).ToList();

    public bool IsRequirementsComplete(List<Requirement> requirements)
        => GetMissingRequirements(requirements).Count <= 0;
}