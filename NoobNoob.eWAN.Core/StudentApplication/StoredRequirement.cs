using NoobNoob.eWAN.Core.Base;

namespace NoobNoob.eWAN.Core.StudentApplication;

/// <summary>
/// Represents a submitted softcopy of a requirement
/// </summary>
public class StoredRequirement : Entity
{
    public Requirement Requirement { get; set; }
    public File SubmittedDocument { get; set; }
}