namespace NoobNoob.eWAN.Core.StudentApplication;

public enum ApplicationStatus
{
    Waiting,
    Accepted,
    Rejected
}