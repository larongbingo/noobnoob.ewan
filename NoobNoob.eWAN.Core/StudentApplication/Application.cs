using System;
using System.Collections.Generic;
using NoobNoob.eWAN.Core.Base;

namespace NoobNoob.eWAN.Core.StudentApplication;

/// <summary>
/// Represents an applicant's application to enroll in the premises
/// </summary>
public class Application : Entity
{
    public Application(User applicant)
    {
        Applicant = applicant;
    }

    public User Applicant { get; }
    public StoredRequirements Requirements { get; private set; } = new StoredRequirements();
    public ApplicationStatus ApplicationStatus { get; private set; } = ApplicationStatus.Waiting;
    public User? Reviewer { get; private set; }
    public string? Comments { get; private set; }

    public void UpdateStatus(User reviewer, ApplicationStatus newStatus, string? comments)
    {
        if (newStatus != ApplicationStatus.Waiting)
            throw new Exception($"Application is already revieweed by {Reviewer.FirstName} {Reviewer.LastName}");

        Reviewer = reviewer;
        ApplicationStatus = newStatus;
        Comments = comments;
    }
}