using NoobNoob.eWAN.Core.Base;

namespace NoobNoob.eWAN.Core.StudentApplication;

/// <summary>
/// A description of a document that needs to be submitted
///</summary>
public class Requirement : Entity
{
    public string Name { get; set; }
    public string? Description { get; set; }
}