using NoobNoob.eWAN.Core.Base;

namespace NoobNoob.eWAN.Core;

/// <summary>
/// Represents a file stored in a blob storage
/// </summary> 
public class File : Entity
{
    public Uri Reference { get; }
    public string Filename { get; set; }
}